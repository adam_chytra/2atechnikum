

from calendar import month

from datetime import datetime



aktualna_data = datetime.now()



class Wydatek:

    def __init__(self,nazwa,kwota,typ="towar",data = datetime):

        self.nazwa = nazwa

        self.kwota = kwota

        self.typ = typ

        self.data = data


    def __str__(self):

        return(self.nazwa + " " + str(self.kwota) + " " + self.typ + " " + str(self.data.year) + "." + str(self.data.month) + "." + str(self.data.day))







# klasa Lista z atrybutem lista_produktow przechowująca każdy dodany wydatek

class Lista_wydatkow:

    def __init__(self,lista_produktow):

        self.lista_produktow = lista_produktow



    # funkcja wypisująca po kolei wszystkie dodane wydatki

    def wyswietl_wszystkie(self):

        number = 1

        for o in self.lista_produktow:

            print(str(number) + ". " + o.__str__())

            number += 1



    def wyswietl_uslugi(self):

        number = 1

        for o in self.lista_produktow:

            if o.typ == "usluga":

                print(str(number) + ". " + o.__str__())

                number += 1



    def wyswietl_po_dacie(self,rok,miesiac,dzien):

        data = datetime(rok,miesiac,dzien)

        number = 1

        roznica_dni_od_dzisiaj = (aktualna_data - data).days

        for o in self.lista_produktow:

            roznica_dni_od_danej_daty = (o.data - data).days

            if(roznica_dni_od_danej_daty < roznica_dni_od_dzisiaj):

                print(str(number) + ". " + o.__str__())

                number += 1



    def wyswietl_po_kwocie(self,kwota):

        number = 1

        for o in self.lista_produktow:

            if(o.kwota > kwota):

                print(str(number) + ". " + o.__str__())

                number += 1







ob1 = Wydatek("lays",4.50,"towar",datetime(2015,4,17))

ob2 = Wydatek("coca cola",6,"usluga",datetime(2021,12,17))

ob3 = Wydatek("fryzier",25,"usluga",datetime(2021,10,2))



ww= Lista_wydatkow ([ob1,ob2,ob3])

ww.wyswietl_wszystkie()

print()

ww.wyswietl_uslugi()

print()

ww.wyswietl_po_dacie(2020,12,17)

print()

ww.wyswietl_po_kwocie(5)

print()

